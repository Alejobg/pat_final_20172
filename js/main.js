// Usuario de prueba
var user = { id:1, name: "Alejandro" , lastname: "Arboleda", password: "batman", email: "alejandro.arboleda@upb.edu.co", foto: "path", facultad: "Ingenieria Sistemas"};

$( document ).ready(function() {
      $("#lbl_NombreCompleto").text(user.name+" "+user.lastname);
      $("#lbl_ID").text(user.id);
      $("#lbl_Correo").text(user.email);
      $("#lbl_Facultad").text(user.facultad);
     

      //Funcion para el login de usuario
		$("#btn_login").click(function(){
			var email=$("#txt_email").val();
			var password=$("#txt_password").val();
         
			if(identificarUser(email)){
				if(autentificarUser(password)){
          window.location.href='buscar.html';
					$('#form_login').attr('action', 'buscar.html');
				}else{
          $("#Modal_ingreso").modal();
        }
			}else{
        $("#Modal_ingreso").modal();
      }
		});


      //funcion para ingresar al perfil
      $("#li_perfil").click(function(){
      window.location.href='perfil.html';

});

      //funcion para cambiar foto de perfil
      $('#profile-image1').on('click', function() {
        $('#profile-image-upload').click();
    });

       //funcion para regresar de la vista perfil
       $("#btn_regresar").click(function(){
            window.location ="buscar.html";
       }); 

       //funcion para ingresar a editar el perfil
       $("#btn_editar_perfil").click(function(){
            window.location ="editar_perfil.html";
       }); 


       //funcion para editar contraseña
     $("#btn_guardar_cambios").click(function(){
            var passUS=$("#edit_pass").val();
            var passrepS=$("#edit_conf_pass").val();
            
            if(passUS==passrepS){
                window.location ="perfil.html";
            }else{
                $("#Modal_password").modal();

            }
       }); 
      
        //funcion para cancelar cambio de contraseña
       $("#btn_cancelar_cambios").click(function(){
            window.location ="perfil.html";
       });
       //funcion para ingresar a la vista registro
      $("#btn_ingresar_registro").click(function(){
            window.location ="registro.html";
       });

       $("#btn_regresar_login").click(function(){
            window.location ="index.html";
       });

       $("#btn_registrar").click(function(){
             $("#lbl_NombreCompleto").text("Alejo"+" "+"Arboleda");
             $("#lbl_ID").text("2");
             $("#lbl_Correo").text("alejo.begood@upb.edu.co");
             $("#lbl_Facultad").text(user.facultad);
            window.location ="buscar.html";
       });

       $("#btn_login_pasajero").click(function(){
            window.location ="login.html";
       });

        $("#btn_login_conductor").click(function(){
            window.location ="login.html";
       });


   });

   //la funcion se conecta a la base de datos y trae los datos del usuario identificado con el email que
   // se envia como parametro
   function identificarUser(email){
         conectarBD();
         if(email==user.email){
          return true;
         }else{
            return false;
         }

   }
   //la funcion valida si el valor del atributo password de usuario sea igual al valor del password enviado como parametro
   function autentificarUser(password){
         conectarBD();
         if(password==user.password){
            return true;
         }else{
            return false;
         }
      }
    //la funcion se conecta a la base de datos  
   function conectarBD(){}